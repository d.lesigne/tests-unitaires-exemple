#ifndef ARITHMETIQUE_H
#define ARITHMETIQUE_H

namespace arithmetique {
  long int addition(int, int);
  long int soustraction(int, int);
  long int multiplication(int, int);
  long int division(int, int);
}

#endif // ARITHMETIQUE_H
