#include "arithmetique.h"

long int arithmetique::addition(int operandeA, int operandeB)
{
  return static_cast<long int>(operandeA) + operandeB;
}

long int arithmetique::soustraction(int operandeA, int operandeB)
{
  return static_cast<long int>(operandeA) - operandeB;
}

long int arithmetique::multiplication(int operandeA, int operandeB)
{
  return static_cast<long int>(operandeA) * operandeB;
}

long int arithmetique::division(int dividende, int diviseur)
{
  return static_cast<long int>(dividende) / diviseur;
}

