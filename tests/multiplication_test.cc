#include "multiplication_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(multiplicationTest);

void multiplicationTest::setUp() {}

void multiplicationTest::tearDown() {}

void multiplicationTest::multiplication_normal()
{
  operandeA = 2;
  operandeB = 2;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(4),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );
  operandeB = -2;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(-4),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );
}

void multiplicationTest::multiplication_max()
{
  operandeA = std::numeric_limits<int>::max();
  operandeB = 2;
  CPPUNIT_ASSERT_GREATER(static_cast<long int>(arithmetique::multiplication(operandeA, operandeB)),
			 static_cast<long int>(operandeA)
    );
}

void multiplicationTest::multiplication_min()
{
  operandeA = std::numeric_limits<int>::lowest();
  operandeB = -2;
  CPPUNIT_ASSERT_LESS(static_cast<long int>(arithmetique::multiplication(operandeA, operandeB)),
		      static_cast<long int>(operandeA)
    );
}

void multiplicationTest::multiplication_zero()
{
  operandeA = operandeB = 0;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(0),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );
  operandeA = 1;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(0),
                       static_cast<long int>(arithmetique::multiplication(operandeA, operandeB))
    );
}

